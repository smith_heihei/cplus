package com.ifreedomer.cplus.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BlogContentInfo implements Serializable {


    @SerializedName("Description")
    private String _$Description73; // FIXME check this code
    private int CommentAuth;
    private String OutlinkCount;
    private String FileName;
    private String ChannelId;
    private boolean is_digg;
    private boolean IsTop;
    private String AvatarUrl;
    private String PostTime;
    private String ArticleId;
    private String ArticleUrl;
    private String Status;
    private String ArticleMore;
    private String UserName;
    private int Digg;
    private String Bury;
    private String IP;
    private String Title;
    private int ViewCount;
    private String NickName;
    private String TypeCopy;
    private String TextBody;
    private int Type;
    private Object Note;
    private boolean read_need_vip;
    private String BlogId;
    private String UpdateTime;
    private int CommentCount;
    private String Level;
    private List<?> Tags;
    private List<CategoriesBean> Categories;

    public String get_$Description73() {
        return _$Description73;
    }

    public void set_$Description73(String _$Description73) {
        this._$Description73 = _$Description73;
    }

    public int getCommentAuth() {
        return CommentAuth;
    }

    public void setCommentAuth(int CommentAuth) {
        this.CommentAuth = CommentAuth;
    }

    public String getOutlinkCount() {
        return OutlinkCount;
    }

    public void setOutlinkCount(String OutlinkCount) {
        this.OutlinkCount = OutlinkCount;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String FileName) {
        this.FileName = FileName;
    }

    public String getChannelId() {
        return ChannelId;
    }

    public void setChannelId(String ChannelId) {
        this.ChannelId = ChannelId;
    }

    public boolean isIs_digg() {
        return is_digg;
    }

    public void setIs_digg(boolean is_digg) {
        this.is_digg = is_digg;
    }

    public boolean isIsTop() {
        return IsTop;
    }

    public void setIsTop(boolean IsTop) {
        this.IsTop = IsTop;
    }

    public String getAvatarUrl() {
        return AvatarUrl;
    }

    public void setAvatarUrl(String AvatarUrl) {
        this.AvatarUrl = AvatarUrl;
    }

    public String getPostTime() {
        return PostTime;
    }

    public void setPostTime(String PostTime) {
        this.PostTime = PostTime;
    }

    public String getArticleId() {
        return ArticleId;
    }

    public void setArticleId(String ArticleId) {
        this.ArticleId = ArticleId;
    }

    public String getArticleUrl() {
        return ArticleUrl;
    }

    public void setArticleUrl(String ArticleUrl) {
        this.ArticleUrl = ArticleUrl;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public String getArticleMore() {
        return ArticleMore;
    }

    public void setArticleMore(String ArticleMore) {
        this.ArticleMore = ArticleMore;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public int getDigg() {
        return Digg;
    }

    public void setDigg(int Digg) {
        this.Digg = Digg;
    }

    public String getBury() {
        return Bury;
    }

    public void setBury(String Bury) {
        this.Bury = Bury;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public int getViewCount() {
        return ViewCount;
    }

    public void setViewCount(int ViewCount) {
        this.ViewCount = ViewCount;
    }

    public String getNickName() {
        return NickName;
    }

    public void setNickName(String NickName) {
        this.NickName = NickName;
    }

    public String getTypeCopy() {
        return TypeCopy;
    }

    public void setTypeCopy(String TypeCopy) {
        this.TypeCopy = TypeCopy;
    }

    public String getTextBody() {
        return TextBody;
    }

    public void setTextBody(String TextBody) {
        this.TextBody = TextBody;
    }

    public int getType() {
        return Type;
    }

    public void setType(int Type) {
        this.Type = Type;
    }

    public Object getNote() {
        return Note;
    }

    public void setNote(Object Note) {
        this.Note = Note;
    }

    public boolean isRead_need_vip() {
        return read_need_vip;
    }

    public void setRead_need_vip(boolean read_need_vip) {
        this.read_need_vip = read_need_vip;
    }

    public String getBlogId() {
        return BlogId;
    }

    public void setBlogId(String BlogId) {
        this.BlogId = BlogId;
    }

    public String getUpdateTime() {
        return UpdateTime;
    }

    public void setUpdateTime(String UpdateTime) {
        this.UpdateTime = UpdateTime;
    }

    public int getCommentCount() {
        return CommentCount;
    }

    public void setCommentCount(int CommentCount) {
        this.CommentCount = CommentCount;
    }

    public String getLevel() {
        return Level;
    }

    public void setLevel(String Level) {
        this.Level = Level;
    }

    public List<?> getTags() {
        return Tags;
    }

    public void setTags(List<?> Tags) {
        this.Tags = Tags;
    }

    public List<CategoriesBean> getCategories() {
        return Categories;
    }

    public void setCategories(List<CategoriesBean> Categories) {
        this.Categories = Categories;
    }

    public static class CategoriesBean {
        /**
         * CategoryId : 286009
         * IsHide : false
         * BlogId : 30686
         * OrderNumber : 0
         * Name : 算法和数据结构
         */

        private String CategoryId;
        private boolean IsHide;
        private String BlogId;
        private String OrderNumber;
        private String Name;

        public String getCategoryId() {
            return CategoryId;
        }

        public void setCategoryId(String CategoryId) {
            this.CategoryId = CategoryId;
        }

        public boolean isIsHide() {
            return IsHide;
        }

        public void setIsHide(boolean IsHide) {
            this.IsHide = IsHide;
        }

        public String getBlogId() {
            return BlogId;
        }

        public void setBlogId(String BlogId) {
            this.BlogId = BlogId;
        }

        public String getOrderNumber() {
            return OrderNumber;
        }

        public void setOrderNumber(String OrderNumber) {
            this.OrderNumber = OrderNumber;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }
    }
}
