package com.ifreedomer.cplus.http.protocol.resp;

public class ForumReplyDetail {

    /**
     * is_buryed : false
     * created_at : 2019-05-04 19:29:21
     * digg : 0
     * avatar : https://profile.csdnimg.cn/9/7/9/0_qq_44728813
     * body : 你那个字符串是什么，必须是True或者False才能转换成功。
     * point : 0
     * post_id : 403865442
     * bury : 0
     * user_id : 75509315
     * nickname : qq_48154268
     * is_digged : false
     * floor : 1
     * username : qq_44728813
     */

    private boolean is_buryed;
    private String created_at;
    private int digg;
    private String avatar;
    private String body;
    private int point;
    private int post_id;
    private int bury;
    private int user_id;
    private String nickname;
    private boolean is_digged;
    private int floor;
    private String username;

    public boolean isIs_buryed() {
        return is_buryed;
    }

    public void setIs_buryed(boolean is_buryed) {
        this.is_buryed = is_buryed;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getDigg() {
        return digg;
    }

    public void setDigg(int digg) {
        this.digg = digg;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public int getBury() {
        return bury;
    }

    public void setBury(int bury) {
        this.bury = bury;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public boolean isIs_digged() {
        return is_digged;
    }

    public void setIs_digged(boolean is_digged) {
        this.is_digged = is_digged;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
