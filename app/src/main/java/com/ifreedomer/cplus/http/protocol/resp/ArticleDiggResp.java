package com.ifreedomer.cplus.http.protocol.resp;

import java.util.List;

public class ArticleDiggResp {

    /**
     * count : 30
     * list : [{"UserName":"qq_44828195","nickName":"你个小鸹貔","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/5/9/1/0_qq_44828195","AddTime":"2019-05-06 01:03:36 570"},{"UserName":"weixin_44802604","nickName":"敲石马农","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/8/6/A/0_weixin_44802604","AddTime":"2019-05-06 00:58:05 265"},{"UserName":"qq_45004551","nickName":"哈哈策略","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/E/B/8/0_qq_45004551","AddTime":"2019-05-06 00:52:22 594"},{"UserName":"qq_40535068","nickName":"qq_40535068","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/8/B/6/0_qq_40535068","AddTime":"2019-05-06 00:44:05 291"},{"UserName":"qq_42729497","nickName":"﹏ㄨ那一场粉色的樱花雨","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/F/0/3/0_qq_42729497","AddTime":"2019-05-06 00:31:07 811"},{"UserName":"weixin_43604927","nickName":"陆次方","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/B/3/C/0_weixin_43604927","AddTime":"2019-05-06 00:28:11 491"},{"UserName":"biggerchong","nickName":"知飞翀","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/8/2/B/0_biggerchong","AddTime":"2019-05-05 23:59:40 850"},{"UserName":"weixin_38404507","nickName":"Alex要努力","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/9/C/F/0_weixin_38404507","AddTime":"2019-05-05 23:58:33 442"},{"UserName":"qq_41010127","nickName":"qq_41010127","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/A/0/F/0_qq_41010127","AddTime":"2019-05-05 23:50:50 534"},{"UserName":"weixin_43421459","nickName":"张意晓","focus":false,"ArticleId":"89670703","avatar":"https://profile.csdnimg.cn/4/B/E/0_weixin_43421459","AddTime":"2019-05-05 23:41:37 444"}]
     */

    private int count;
    private List<ListBean> list;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * UserName : qq_44828195
         * nickName : 你个小鸹貔
         * focus : false
         * ArticleId : 89670703
         * avatar : https://profile.csdnimg.cn/5/9/1/0_qq_44828195
         * AddTime : 2019-05-06 01:03:36 570
         */

        private String UserName;
        private String nickName;
        private boolean focus;
        private String ArticleId;
        private String avatar;
        private String AddTime;

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public boolean isFocus() {
            return focus;
        }

        public void setFocus(boolean focus) {
            this.focus = focus;
        }

        public String getArticleId() {
            return ArticleId;
        }

        public void setArticleId(String ArticleId) {
            this.ArticleId = ArticleId;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getAddTime() {
            return AddTime;
        }

        public void setAddTime(String AddTime) {
            this.AddTime = AddTime;
        }
    }
}
