package com.ifreedomer.cplus.http.protocol;

import com.ifreedomer.cplus.http.protocol.resp.ForumDetailResp;
import com.ifreedomer.cplus.http.protocol.resp.ForumDiggResp;
import com.ifreedomer.cplus.http.protocol.resp.ForumHotResp;
import com.ifreedomer.cplus.http.protocol.resp.ForumPostResp;
import com.ifreedomer.cplus.http.protocol.resp.ForumReplyDetail;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ForumApi {
    @GET("cms-app/v1/bbs/new_hot_topics")
    Observable<PayLoad<List<ForumHotResp>>> getHotForum(@Query("topicType") String topicType, @Query("page") int page);

    @GET("/cms-app/v1/bbs/get_topic_details")
    Observable<PayLoad<List<ForumDetailResp>>> getForumDetail(@Query("topic_id") String topicId
            , @Query("body_format") String bodyFormap
    );



    @GET("/cms-app/v1/bbs/list_reply_topic")
    Observable<PayLoad<List<ForumReplyDetail>>> getCommentList(@Query("topicId") String topicId
            , @Query("page") int page, @Query("size") int size
    );


    @POST("cms-app/v1/bbs/login/create_digg")
    Observable<PayLoad<ForumDiggResp>> digg(@Body RequestBody body);


    @POST("cms-app/v1/bbs/login/create_reply_topic")
    Observable<PayLoad<ForumPostResp>> forumPost(@Body RequestBody body);


    @GET("api/v2/bbs/report")
    Observable<PayLoad<Boolean>> forumReport(@Query("reason_type") int reasonType, @Query("username") String username, @Query("topic_id") String topicId, @Query("post_id") String postId);

    @POST("api/v3/bbs/create_topic")
    Observable<String> forumCreateTopic(@Body RequestBody formBody);

}
