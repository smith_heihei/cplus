package com.ifreedomer.cplus.http.protocol;

import com.ifreedomer.cplus.http.protocol.resp.AddCommentResp;
import com.ifreedomer.cplus.http.protocol.resp.CommentListResp;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CommentApi {


//    cms-app/v1/blog_details/get_commnet_list?size=30&articleId=89670703&page=

    //    /api/v5/Comment/list?article_id=72834303&page=1&size=30
    @GET("cms-app/v1/blog_details/get_commnet_list")
    Observable<PayLoad<CommentListResp>> getCommentList(@Query("articleId") String articleId, @Query("page") int page, @Query("size") int size);


    @POST("cms-app/v1/blog_details/login/add_comment")
    Observable<PayLoad<AddCommentResp>> addComment(@Body RequestBody body);


}
