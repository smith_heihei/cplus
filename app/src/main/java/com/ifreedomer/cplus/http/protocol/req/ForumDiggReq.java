package com.ifreedomer.cplus.http.protocol.req;

public class ForumDiggReq {

    /**
     * postId : 403865442
     * topicId : 392597939
     */

    private String postId;
    private String topicId;

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }
}
