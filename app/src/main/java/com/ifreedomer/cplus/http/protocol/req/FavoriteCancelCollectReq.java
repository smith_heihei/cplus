package com.ifreedomer.cplus.http.protocol.req;

public class FavoriteCancelCollectReq {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
