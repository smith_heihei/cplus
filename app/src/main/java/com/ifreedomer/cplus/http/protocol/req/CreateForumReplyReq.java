package com.ifreedomer.cplus.http.protocol.req;

public class CreateForumReplyReq {
    public String topicId;
    public String body;

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
