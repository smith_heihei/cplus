package com.ifreedomer.cplus.http.protocol.req;

public class AddCommentReq {
    private String content;
    private String articleId;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }
}
