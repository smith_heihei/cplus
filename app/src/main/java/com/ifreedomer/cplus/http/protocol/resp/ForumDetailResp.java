package com.ifreedomer.cplus.http.protocol.resp;

public class ForumDetailResp {


    /**
     * created_at : 2019-05-04 12:51:32
     * title : C#winform中bool类型问题
     * score : 20
     * bury_count : 0
     * is_tech : true
     * nickname : Xie_Yawen
     * parent_forum : .NET技术
     * id : 392597939
     * floor : 14
     * introduction : user.IsAdmin = bool.Parse(this.cboIsAdmin.Selected
     * forum_urlname : CSharp
     * is_buryed : false
     * avatar : //profile.csdnimg.cn/C/E/7/3_xie_yawen
     * text_body : user.IsAdmin&nbsp;=&nbsp;bool.Parse(this.cboIsAdmin.SelectedValue.ToString());&nbsp;这句话老是报错“System.FormatException”类型的未经处理的异常在&nbsp;mscorlib.dll&nbsp;中发生&nbsp;&nbsp;&nbsp;其他信息:&nbsp;该字符串未被识别为有效的布尔值。<br />
     <br />
     是哪个地方写错了吗？<br />
     * is_top : false
     * url : https://bbs.csdn.net/topics/392597939
     * forum : C#
     * is_expired : false
     * post_id : 403864456
     * digg_count : 1
     * is_recommend : false
     * parent_forum_urlname : DotNET
     * post_count : 14
     * contain_image : false
     * is_digged : false
     * last_post_created_at : 2019-05-06 12:07:10
     * view_count : 903
     * status : 0
     * username : Xie_Yawen
     */

    private String created_at;
    private String title;
    private int score;
    private int bury_count;
    private boolean is_tech;
    private String nickname;
    private String parent_forum;
    private int id;
    private int floor;
    private String introduction;
    private String forum_urlname;
    private boolean is_buryed;
    private String avatar;
    private String text_body;
    private boolean is_top;
    private String url;
    private String forum;
    private boolean is_expired;
    private int post_id;
    private int digg_count;
    private boolean is_recommend;
    private String parent_forum_urlname;
    private int post_count;
    private boolean contain_image;
    private boolean is_digged;
    private String last_post_created_at;
    private int view_count;
    private int status;
    private String username;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getBury_count() {
        return bury_count;
    }

    public void setBury_count(int bury_count) {
        this.bury_count = bury_count;
    }

    public boolean isIs_tech() {
        return is_tech;
    }

    public void setIs_tech(boolean is_tech) {
        this.is_tech = is_tech;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getParent_forum() {
        return parent_forum;
    }

    public void setParent_forum(String parent_forum) {
        this.parent_forum = parent_forum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getForum_urlname() {
        return forum_urlname;
    }

    public void setForum_urlname(String forum_urlname) {
        this.forum_urlname = forum_urlname;
    }

    public boolean isIs_buryed() {
        return is_buryed;
    }

    public void setIs_buryed(boolean is_buryed) {
        this.is_buryed = is_buryed;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getText_body() {
        return text_body;
    }

    public void setText_body(String text_body) {
        this.text_body = text_body;
    }

    public boolean isIs_top() {
        return is_top;
    }

    public void setIs_top(boolean is_top) {
        this.is_top = is_top;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getForum() {
        return forum;
    }

    public void setForum(String forum) {
        this.forum = forum;
    }

    public boolean isIs_expired() {
        return is_expired;
    }

    public void setIs_expired(boolean is_expired) {
        this.is_expired = is_expired;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public int getDigg_count() {
        return digg_count;
    }

    public void setDigg_count(int digg_count) {
        this.digg_count = digg_count;
    }

    public boolean isIs_recommend() {
        return is_recommend;
    }

    public void setIs_recommend(boolean is_recommend) {
        this.is_recommend = is_recommend;
    }

    public String getParent_forum_urlname() {
        return parent_forum_urlname;
    }

    public void setParent_forum_urlname(String parent_forum_urlname) {
        this.parent_forum_urlname = parent_forum_urlname;
    }

    public int getPost_count() {
        return post_count;
    }

    public void setPost_count(int post_count) {
        this.post_count = post_count;
    }

    public boolean isContain_image() {
        return contain_image;
    }

    public void setContain_image(boolean contain_image) {
        this.contain_image = contain_image;
    }

    public boolean isIs_digged() {
        return is_digged;
    }

    public void setIs_digged(boolean is_digged) {
        this.is_digged = is_digged;
    }

    public String getLast_post_created_at() {
        return last_post_created_at;
    }

    public void setLast_post_created_at(String last_post_created_at) {
        this.last_post_created_at = last_post_created_at;
    }

    public int getView_count() {
        return view_count;
    }

    public void setView_count(int view_count) {
        this.view_count = view_count;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
