package com.ifreedomer.cplus.http.protocol.resp;

public class CheckCollectResp {

    /**
     * is_exist : 1
     * msg : 您已经收藏过
     * favorite_id : 18658151
     */

    private boolean isExist;
    private String msg;
    private String favorite_id;

    public boolean isExist() {
        return isExist;
    }

    public void setExist(boolean exist) {
        isExist = exist;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getFavorite_id() {
        return favorite_id;
    }

    public void setFavorite_id(String favorite_id) {
        this.favorite_id = favorite_id;
    }
}
