package com.ifreedomer.cplus.http.protocol.req;

public class DiggReq {
    private String articleId;

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }
}
